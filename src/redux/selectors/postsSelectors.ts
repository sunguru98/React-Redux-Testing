import { ReduxStore } from '../utils/interfaces'

export const selectPosts = (state: ReduxStore) => state.posts
